# StagingPilot Portal #
**Contributors:**      StagingPilot  
**Donate link:**       https://stagingpilot.com  
**Tags:**  
**Requires at least:** 4.4  
**Tested up to:**      4.7.2 
**Stable tag:**        0.0.0  
**License:**           GPLv2  
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html  

## Description ##

Add StagingPilot functionality to your site

### Adds shortcode ###
[stagingpilot_updates site="mysite"]

defaults to updates in the current calendar month, but date ranges can be specified as well:
[stagingpilot_updates site="mysite" start_date="2017-01-01" end_date="2017-12-31"]

### Adds Convenience functions ###
get_stagingpilot_updates( array $args )

#### Example Usage ####
$updates_on_mysite_this_year = get_stagingpilot_updates( array(
	'site' => 'mysite',
	'start_date' => '2017-01-01',
	'end_date' => '2017-12-31',
) );

## Installation ##

You need to define a STAGINGPILOT_API_KEY

### Manual Installation ###

1. Upload the entire `/stagingpilot-portal` directory to the `/wp-content/plugins/` directory.
2. Activate StagingPilot Portal through the 'Plugins' menu in WordPress.

## Frequently Asked Questions ##


## Screenshots ##


## Changelog ##

### 0.0.0 ###
* First release

## Upgrade Notice ##

### 0.0.0 ###
First Release
