# StagingPilot Portal Assets #
https://stagingpilot.com
Copyright (c) 2017 StagingPilot
Licensed under the GPLv2 license.

Assets such as styles, javascript, and images.