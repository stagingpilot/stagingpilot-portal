<?php
/**
 * StagingPilot Portal Functions.
 *
 * @since   0.0.0
 * @package StagingPilot_Portal
 */

/**
 * StagingPilot Portal Functions.
 *
 * @since 0.0.0
 */
class SPP_Functions {
	/**
	 * Parent plugin class.
	 *
	 * @since 0.0.0
	 *
	 * @var   StagingPilot_Portal
	 */
	protected $plugin = null;

	/**
	 * Constructor.
	 *
	 * @since  0.0.0
	 *
	 * @param  StagingPilot_Portal $plugin Main plugin object.
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks.
	 *
	 * @since  0.0.0
	 */
	public function hooks() {
		
	}

	public function get_updates( $args ) {
		if ( ! $this->plugin->meets_config_requirements() ) {
			return false;
		}

		$args = shortcode_atts( array(
			'site' => '',
			'start_date' => date( 'Y-m-01' ),
			'end_date' => date( 'Y-m-t' ),
		), $args );

		$url = StagingPilot_Portal::API_ROOT . '/updates';
		$response = wp_remote_get( $url, array(
			'body' => array(
				'api_key' => STAGINGPILOT_API_KEY,
				'projectslug' => $args['site'],
				'start_date' => $args['start_date'],
				'end_date' => $args['end_date'],
			)
		) );
		$json = wp_remote_retrieve_body( $response );

		$updates = json_decode( $json, true );
		$summary = $this->plugin->functions->summarize( $updates );
		$consolidated_summary = $this->plugin->functions->prepare_summary_data( $summary );


		return $consolidated_summary;
	}

	public function summarize( $data ) {
		$consolidated_logs = array();

		foreach ( $data as $log ) {
			$unique_key = $log['name'];
			$consolidated_logs[$unique_key]['all'][] = $log;
			$consolidated_logs[$unique_key]['summary'] = $log;
		}

		foreach ( $consolidated_logs as $unique_key => &$consolidated_log ) {
			// foreach ( $consolidated_log['all'] as $single_consolidated_log_key => &$single_consolidated_log ) {
			// 	$single_consolidated_log = $this->prepare_item_for_response( $single_consolidated_log, $request );
			// }
			$first_log = $consolidated_log['all']['0'];
			// $consolidated_log['summary'] = $this->prepare_item_for_response( $consolidated_log['summary'], $request );
			$consolidated_log['summary']['version_before'] = $first_log['version_before'];
			$consolidated_log['summary']['earliest_date_created'] = $first_log['date_created'];
			$consolidated_log['summary']['count'] = count( $consolidated_logs[$unique_key]['all'] );
		}

		return $consolidated_logs;
	}

	public function prepare_summary_data( $consolidated_logs ) {
		$summary_data = array();
		$updated_actions = array(
			'plugin' => 'plugins',
			'theme' => 'themes',
			'core' => 'core',
		);
		foreach ($consolidated_logs as $site_id => $consolidated_log) {
			$summary = $consolidated_log['summary'];
			foreach ($updated_actions as $type => $value) {
				if ($summary['type'] == $type ) {
					$summary_data['updates'][$value]['list'][$summary['name']] = array(
						'name' => str_ireplace('Updated '.$type.' ', '', $summary['title']),
						'version_before' => $summary['version_before'],
						'version_after' => $summary['version_after'],
					);
					$summary_data['updates'][$value]['list'][$summary['name']]['name'] = str_ireplace('Updated pro '.$type.' ', '', $summary_data['updates'][$value]['list'][$summary['name']]['name'] );
					if ($type == 'core') {
						$summary_data['updates'][$value]['list'][$summary['name']]['name'] = 'Core';
					}
				}
			}
		}

		if ( !empty( $summary_data['updates'] ) ) {		
			foreach ($summary_data['updates'] as &$value) {
				$value['count'] = count( $value['list'] );
			}
		}

		return $summary_data;
	}

	public function prepare_formatted_output( $summary_data, $params ) {
		$formatted_message ='';

		if ($params['start_date'] && $params['end_date']) {
			$formatted_message .= 'Between '.date( 'm/d', strtotime( $params['start_date'] ) ).' and '.date( 'm/d', strtotime( $params['end_date'] ) ).', ';
		}

		$formatted_message .= 'we updated ';

		if ($summary_data['updates']['plugins']['count'] > 0) {
			$formatted_message .= $summary_data['updates']['plugins']['count'].' plugin';
			if ( $summary_data['updates']['plugins']['count'] > 1 ) {
				$formatted_message .= 's';
			}
		}
		if ($summary_data['updates']['plugins']['count'] > 0 && $summary_data['updates']['themes']['count'] > 0) {
			$formatted_message .=  ' and ';
		}
		if ($summary_data['updates']['themes']['count'] > 0) {
			$formatted_message .=  $summary_data['updates']['themes']['count'].' theme';
			if ( $summary_data['updates']['themes']['count'] > 1 ) {
				$formatted_message .= 's';
			}
		}
		$formatted_message .= '.'.PHP_EOL;

		foreach ( array('plugins', 'themes') as $update ) {
			if ( $summary_data['updates'][$update]['count'] > 0 ) {
				$updates_string = 'The '.$update.' we updated were: ';
				if ( $summary_data['updates'][$update]['count'] < 2 ) {
					$updates_string = str_replace( array( $update, 'were' ), array(str_replace( 's', '', $update ), 'was' ), $updates_string );
				}
				$formatted_message .= PHP_EOL.$updates_string.PHP_EOL;
				sort($summary_data['updates'][$update]['list']);
				foreach ($summary_data['updates'][$update]['list'] as $update_list) {
					$formatted_message .= '* '.$update_list['name'];
					if ( $params['display_version_numbers']
					 && !empty( $update_list['version_before'] )
					  && !empty( $update_list['version_after'] )
					) {
						$formatted_message .= ' (v'.$update_list['version_before'].' -> v'.$update_list['version_after'].')';
					}
					$formatted_message .= PHP_EOL;
				}
			}
		}

		if ( $summary_data['updates']['core']['count'] > 0 ) {
			$formatted_message .= 'We also updated WordPress Core from '.$summary_data['updates']['core']['list']['core']['version_before'].' to '.$summary_data['updates']['core']['list']['core']['version_after'].'.'.PHP_EOL;
		}

		return $formatted_message;
	}
}

/* Global function wrappers */
function get_stagingpilot_updates( $args ) {
	return StagingPilot_Portal::get_instance()->functions->get_updates( $args );
}