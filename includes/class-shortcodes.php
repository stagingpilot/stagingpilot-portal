<?php
/**
 * StagingPilot Portal Shortcodes.
 *
 * @since   0.0.0
 * @package StagingPilot_Portal
 */

/**
 * StagingPilot Portal Shortcodes.
 *
 * @since 0.0.0
 */
class SPP_Shortcodes {
	/**
	 * Parent plugin class.
	 *
	 * @since 0.0.0
	 *
	 * @var   StagingPilot_Portal
	 */
	protected $plugin = null;

	/**
	 * Constructor.
	 *
	 * @since  0.0.0
	 *
	 * @param  StagingPilot_Portal $plugin Main plugin object.
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks.
	 *
	 * @since  0.0.0
	 */
	public function hooks() {
		add_shortcode( 'stagingpilot_updates', array( $this, 'stagingpilot_updates' ) );
	}

	public function stagingpilot_updates( $atts ) {
		$atts = shortcode_atts( array(
			'site' => '',
			'start_date' => date( 'Y-m-01' ),
			'end_date' => date( 'Y-m-t' ),
			'display_version_numbers' => true,
		), $atts, 'stagingpilot_updates' );
		$atts = array_filter( $atts );

		if ( empty( $atts['site'] ) ) {
			return '<code>site</code> is a required parameter. Example: <code>[stagingpilot_updates site="mysite"]</code>';
		}

		$updates = $this->plugin->functions->get_updates( $atts );
		$formatted_output = $this->plugin->functions->prepare_formatted_output( $updates, $atts );
		$formatted_output = nl2br( $formatted_output );

		return $formatted_output;
	}
}
